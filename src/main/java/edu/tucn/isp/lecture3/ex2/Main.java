package edu.tucn.isp.lecture3.ex2;

import java.util.Scanner;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Choose: 1. Type 1, 2.Type 2");
        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();

        Car car;

        switch (choice) {
            case 1:
                car = new Type1("Type 1");
                break;

            case 2:
                car = new Type2("Type 2");
                break;

            default:
                throw new IllegalArgumentException("Bad choice!");
        }

        car.start();
        car.move();
        car.stop();

        System.out.println("Game over");
    }
}
