package edu.tucn.isp.lecture3.ex2;

/**
 * @author Radu Miron
 * @version 1
 */
public class Type1 extends Car {

    public Type1(String model) {
        super(model);
    }

    @Override
    public void move() {
        super.move();
        System.out.println(" slower");
    }
}
