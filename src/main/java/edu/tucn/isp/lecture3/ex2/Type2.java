package edu.tucn.isp.lecture3.ex2;

/**
 * @author Radu Miron
 * @version 1
 */
public class Type2 extends Car {

    public Type2(String model) {
        super(model);
    }

    @Override
    public void move() {
        super.move();
        System.out.println(" faster");
    }
}
