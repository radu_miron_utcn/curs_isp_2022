package edu.tucn.isp.lecture3.ex2;

/**
 * @author Radu Miron
 * @version 1
 */
public class Car {
    private String model;

    public Car(String model) {
        this.model = model;
    }

    public void start() {
        System.out.println(this.model + " starts");
    }

    public void move() {
        System.out.print(this.model + " moves");
    }

    public void stop() {
        System.out.println(this.model + " stops");
    }
}
