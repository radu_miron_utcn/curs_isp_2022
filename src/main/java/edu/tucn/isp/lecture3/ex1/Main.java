package edu.tucn.isp.lecture3.ex1;

/**
 * @author Radu Miron
 * @version 1
 */
class Person { // clasa de baza
    private String name;

    public Person(String name) { // (2)
        this.name = name;
    }

    public void printName() {
        System.out.println(this.name);
    }
}

class Student extends Person { // clasa derivata
    private String registrationNumber;

    public Student(String name, String registrationNumber) { // (3)
        super(name);
        this.registrationNumber = registrationNumber;
    }

    public void printRegNum() {
        System.out.println(this.registrationNumber);
    }
}

class Teacher extends Person { // clasa derivata
    private String university;

    public Teacher(String name, String university) { // (3)
        super(name);
        this.university = university;
    }

    public void printUniversity() {
        System.out.println(this.university);
    }

    @Override
    public void printName() {
        System.out.print(this.university + ": ");
        super.printName();
    }
}

public class Main {
    public static void main(String[] args) {
        Student s1 = new Student("John", "123"); // (1)
        s1.printName();
        s1.printRegNum();

        Teacher t1 = new Teacher("Mike", "TUCN");
        t1.printName();

        Person p1 = s1;
        Student s2 = (Student) p1;

        Person p2 = new Person("Jim");
        Student s3 = (Student) p2;
    }
}
