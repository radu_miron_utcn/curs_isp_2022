package edu.tucn.isp.lecture7;

import com.google.gson.Gson;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * @author Radu Miron
 * @version 1
 */
public class Ex4JsonSerialization {
    public static void main(String[] args) throws IOException {
        Gson gson = new Gson();

        System.out.println("1.Serialize (Write object to file) / 2.Deserialize (Read object from file)");

        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();

        if (choice == 1) {
            Controller controller = new Controller(1, "Omron", "PLC-1");
            String controllerJson = gson.toJson(controller);
            Files.write(Paths.get("testfiles/"+controller.getId()+".json"), controllerJson.getBytes());
        } else if (choice == 2){
            Controller controller = gson.fromJson(new FileReader("testfiles/1.json"), Controller.class);
            System.out.println(controller);
        } else {
            throw new IllegalArgumentException("Invalid choice");
        }
    }

    private static class Controller {
        private int id;
        private String vedor;
        private String model;

        public Controller(int id, String vedor, String model) {
            this.vedor = vedor;
            this.model = model;
            this.id = id;
        }

        public String getVedor() {
            return vedor;
        }

        public void setVedor(String vedor) {
            this.vedor = vedor;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        @Override
        public String toString() {
            return "Controller{" +
                    "id=" + id +
                    ", vedor='" + vedor + '\'' +
                    ", model='" + model + '\'' +
                    '}';
        }
    }
}
