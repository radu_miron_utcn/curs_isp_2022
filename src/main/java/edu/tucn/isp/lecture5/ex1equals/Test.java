package edu.tucn.isp.lecture5.ex1equals;

import java.util.Scanner;

/**
 * @author Radu Miron
 * @version 1
 */
public class Test {
    public static void main(String[] args) {
//        String a = "abc";
//        String b = "abc";
//
//        System.out.println(a == b);
//        System.out.println(a.equals(b));
//
//        System.out.println("Enter abc:");
//        Scanner scanner = new Scanner(System.in);
//        String c = scanner.nextLine();
//
//        System.out.println(String.format("%s=%s ? %b", a, c, a == c));
//        System.out.println(String.format("%s=%s ? %b", a, c, a.equals(c)));
//
//        Integer i1 = 127;
//        System.out.println("Enter 128");
//        Integer i2 = scanner.nextInt();
//
//        System.out.println(i1 == i2);

        PersonType personType = PersonType.valueOf(101);
        System.out.println(personType);
    }
}
