package edu.tucn.isp.lecture5.ex1interf;

import javax.swing.*;
import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win extends JFrame {
    public Win() {
        int x = new Random().nextInt(1000) + 10;
        int y = new Random().nextInt(1000) + 10;
        this.setBounds(x, y, 200, 100);

        JButton button = new JButton("Click to close!");
        ButtonHandler buttonHandler = new ButtonHandler();
        button.addActionListener(buttonHandler);

        this.add(button);

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(true);
    }
}
