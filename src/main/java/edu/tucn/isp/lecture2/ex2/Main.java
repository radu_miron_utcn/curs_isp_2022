package edu.tucn.isp.lecture2.ex2;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {

    public static void main(String[] args) {
        Car car1 = new Car("BMW 320d", "black", 240);
        Car car2 = new Car("Renault Symbol", "blue", 170);

        car1.move();
        car2.move();
        car2.move(100);

        Car car3 = car1; // 2 variables refer the same object!

        car1.crash(car2);
        car2.crash(car1);

        System.out.println(Car.NUMBER_OF_WHEELS);
        Car.printNumberOfWheels();

        car1.setColor("green");
    }
}
