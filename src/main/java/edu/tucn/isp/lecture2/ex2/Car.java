package edu.tucn.isp.lecture2.ex2;

/**
 * @author Radu Miron
 * @version 1
 */
public class Car {
    //static variable
    public static final int NUMBER_OF_WHEELS = 4;

    //state section: attributes
    private String model;
    private String color;
    private int topSpeed;

    //constructor
    public Car(String model, String color, int topSpeed) {
        this.model = model;
        this.color = color;
        this.topSpeed = topSpeed;
    }

    public static void printNumberOfWheels() {
        System.out.println(String.format("All the cars have %s wheels", Car.NUMBER_OF_WHEELS));
    }

    //dynamic section: methods
    public void move() {
        System.out.println(this.model + " of color " + this.color + " moves for " + this.getRandomDistance() + " m");
    }

    public void move(int d) {
        System.out.println(this.model + " of color " + this.color + " moves for " + d + " m");
    }

    private int getRandomDistance() {
        return (int) Math.round(Math.random() * 7 + 5);
    }

    public void crash(Car anotherCar) {
        System.out.println(this.model + " crashes into " + anotherCar.model);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
