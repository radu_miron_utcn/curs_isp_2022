package edu.tucn.isp.lecture2.ex1;

import edu.tucn.isp.lecture2.ex2.Car;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static int countA(String testString) {
        int c = 0;

        for (int i = 0; i < testString.length(); i++) {
            if (testString.charAt(i) == 'a') {
                c++;
            }
        }

        return c;
    }

    public static void main(String[] args) {
//        System.out.println("Input your text:");
//        Scanner scanner = new Scanner(System.in);
//        String testString = scanner.nextLine();
//
//        System.out.println(countA(testString));
        Car car = new Car("", "", 0);
        Car car1 = new Car("", "", 0);
        car = car1;
    }
}
