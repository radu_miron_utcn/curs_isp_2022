package edu.tucn.isp.lecture6.ex1collections.subex3map;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Radu Miron
 * @version 1
 */
public class Ex0HashMap {
    public static void main(String[] args) {
        Map<String, String> dictionary = new HashMap<>();
        dictionary.put("test", "this is a test");
        dictionary.put("color", "this is a color");

        String value1 = dictionary.get("test");
        System.out.println(value1);

        String value2 = dictionary.get("something");
        System.out.println(value2);
    }
}
