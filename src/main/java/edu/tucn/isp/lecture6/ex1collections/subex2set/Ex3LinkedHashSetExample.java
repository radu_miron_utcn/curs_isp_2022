package edu.tucn.isp.lecture6.ex1collections.subex2set;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author Radu Miron
 * @version 1
 */
public class Ex3LinkedHashSetExample {
    public static void main(String[] args) {
        // todo implement some examples
        Set<String> set = new HashSet<>();
        set.add("cccc");
        set.add("aaaa");
        set.add("bbbb");
        set.add("0000");

//        set.forEach(s -> System.out.println(s));

        Set<String> linkedSet = new LinkedHashSet<>();
        linkedSet.add("cccc");
        linkedSet.add("aaaa");
        linkedSet.add("bbbb");
        linkedSet.add("0000");
        linkedSet.forEach(s-> System.out.println(s));
    }
}
