package edu.tucn.isp.lecture6.ex1collections.subex1list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Radu Miron
 * @version 1
 */
public class Junk {
    public static void main(String[] args) {
        List<String> strings = new ArrayList<>();

        strings.add("a");
        strings.add("b");
        strings.add("c");
        strings.add("c");
        strings.add("f");

//        for (String s : strings) {
//            if (s.equals("b")) {
//                strings.remove(s);
//            }
//        }

        Iterator<String> iterator = strings.iterator();
        while (iterator.hasNext()) {
            String current = iterator.next();

            if (current.equals("f")) {
//                iterator.remove();
                strings.add("z");
            } else {
            }
        }

        System.out.println();
    }
}
