package edu.tucn.isp.lecture6.ex1collections.subex3map;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Radu Miron
 * @version 1
 */
public class Colllision {
    public static void main(String[] args) {
        System.out.println("BB".hashCode());
        System.out.println("Aa".hashCode());

        Map<String, String> map = new HashMap<>();
        map.put("Aa", "test");
        map.put("Aa", "test1");
        map.put("BB", "test1");
        map.forEach((k, v) -> System.out.println(k + " - " + v));
    }
}
