package edu.tucn.isp.lecture10.ex5deadlock;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyThread extends Thread {
    private Object lock1;
    private Object lock2;

    public MyThread(Object lock1, Object lock2) {
        this.lock1 = lock1;
        this.lock2 = lock2;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " do activity 1");

        synchronized (lock1) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException ignored) {
            }
            synchronized (lock2) {
                System.out.println(Thread.currentThread().getName() + " do critical activity 2");
            }
        }

        System.out.println(Thread.currentThread().getName() + " do activity 3");
    }
}
