package edu.tucn.isp.lecture10.ex2sync;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyThread extends Thread {
    private static final Object LOCK = new Object();

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " do activity 1");

        synchronized (LOCK) {
            System.out.println(Thread.currentThread().getName() + " do critical activity 2");
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ignored) {
            }
        }

        System.out.println(Thread.currentThread().getName() + " do activity 3");
    }
}
