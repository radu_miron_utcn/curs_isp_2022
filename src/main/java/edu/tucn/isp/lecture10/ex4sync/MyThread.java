package edu.tucn.isp.lecture10.ex4sync;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyThread extends Thread {
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " do activity 1");

        Activity.doActivity();

        System.out.println(Thread.currentThread().getName() + " do activity 3");
    }
}
