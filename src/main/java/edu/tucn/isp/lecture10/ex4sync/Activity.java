package edu.tucn.isp.lecture10.ex4sync;

/**
 * @author Radu Miron
 * @version 1
 */
public class Activity {
    public static synchronized void doActivity(){
        System.out.println(Thread.currentThread().getName() + " do critical activity 2");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
        }
    }
}
