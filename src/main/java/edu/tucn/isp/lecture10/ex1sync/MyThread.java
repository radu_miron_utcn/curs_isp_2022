package edu.tucn.isp.lecture10.ex1sync;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyThread extends Thread {
    private Object lock;

    public MyThread(Object lock) {
        this.lock = lock;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " do activity 1");

        synchronized (lock) {
            System.out.println(Thread.currentThread().getName() + " do critical activity 2");
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ignored) {
            }
        }

        System.out.println(Thread.currentThread().getName() + " do activity 3");
    }
}
