package edu.tucn.isp.lecture10.ex6waitnotify;

/**
 * @author Radu Miron
 * @version 1
 */
public class Notifier extends Thread {
    private Object monitor;

    public Notifier(Object monitor) {
        this.monitor = monitor;
    }

    @Override
    public void run() {
        System.out.println("Notifier: I will notify the other thread in 5 sec");

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }

        synchronized (monitor) {
            monitor.notify();

            System.out.println("Notifier: I sent the notification!");

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
            }
        }
    }
}
