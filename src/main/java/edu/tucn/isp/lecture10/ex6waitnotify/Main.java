package edu.tucn.isp.lecture10.ex6waitnotify;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Object monitor = new Object();

        new Waiter(monitor).start();
        new Notifier(monitor).start();
    }
}
