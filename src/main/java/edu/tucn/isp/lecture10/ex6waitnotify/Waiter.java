package edu.tucn.isp.lecture10.ex6waitnotify;

/**
 * @author Radu Miron
 * @version 1
 */
public class Waiter extends Thread {
    private Object monitor;

    public Waiter(Object monitor) {
        this.monitor = monitor;
    }

    @Override
    public void run() {
        System.out.println("Waiter: I'm waiting to be notified");

        synchronized (monitor) {
            try {
                monitor.wait();
                // it's like doing synchronized(monitor) all over agin
            } catch (InterruptedException e) {
            }
        }

        System.out.println("I was notified");
    }
}
