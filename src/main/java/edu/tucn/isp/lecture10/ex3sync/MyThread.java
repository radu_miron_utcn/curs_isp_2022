package edu.tucn.isp.lecture10.ex3sync;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyThread extends Thread {
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " do activity 1");

        synchronized (MyThread.class) {
            System.out.println(Thread.currentThread().getName() + " do critical activity 2");
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ignored) {
            }
        }

        System.out.println(Thread.currentThread().getName() + " do activity 3");
    }
}
