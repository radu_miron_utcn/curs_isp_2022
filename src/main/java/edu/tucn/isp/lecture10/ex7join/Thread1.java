package edu.tucn.isp.lecture10.ex7join;

/**
 * @author Radu Miron
 * @version 1
 */
public class Thread1 extends Thread {
    private Thread2 t2;

    public Thread1(Thread2 t2) {
        this.t2 = t2;
    }

    @Override
    public void run() {
        System.out.println("T1 started. Waiting for T2 to end!");

        try {
            t2.join();
        } catch (InterruptedException e) {
        }

        System.out.println("T1 ended");
    }
}
