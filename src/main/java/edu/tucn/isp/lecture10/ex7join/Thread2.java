package edu.tucn.isp.lecture10.ex7join;

/**
 * @author Radu Miron
 * @version 1
 */
public class Thread2 extends Thread{
    @Override
    public void run() {
        System.out.println("T2 started");

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
        }

        System.out.println("T2 ended");
    }
}
