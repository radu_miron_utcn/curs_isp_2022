package edu.tucn.isp.lecture10.ex8deadlockjoin;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Thread2 t2 = new Thread2();
        Thread1 t1 = new Thread1(t2);
        t2.setT1(t1);

        t2.start();
        t1.start();
    }
}
