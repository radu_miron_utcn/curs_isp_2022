package edu.tucn.isp.lecture10.ex8deadlockjoin;

/**
 * @author Radu Miron
 * @version 1
 */
public class Thread2 extends Thread{
    private Thread1 t1;

    @Override
    public void run() {
        System.out.println("T2 started. Waiting for T1 to end!");

        try {
            t1.join();
        } catch (InterruptedException e) {
        }

        System.out.println("T2 ended");
    }

    public void setT1(Thread1 t1) {
        this.t1 = t1;
    }
}
