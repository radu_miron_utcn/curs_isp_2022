package edu.tucn.isp.lecture4.shoppingcart;

/**
 * @author Radu Miron
 * @version 1
 */
public class Product {
    private String name;
    private float price;

    public Product(String name, int price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }

}
