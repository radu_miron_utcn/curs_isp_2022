package edu.tucn.isp.lecture4.shoppingcart;

/**
 * @author Radu Miron
 * @version 1
 */
public class Customer {
    private String address;
    private String phone;
    private ShoppingCart shoppingCart;

    public Customer(String address, String phone, ShoppingCart shoppingCart) {
        this.address = address;
        this.phone = phone;
        this.shoppingCart = shoppingCart;
    }
}
