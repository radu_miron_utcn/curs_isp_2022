package edu.tucn.isp.lecture4.shoppingcart;

/**
 * @author Radu Miron
 * @version 1
 */
public class Article {
    private Product product;
    private int quantity;

    public Article(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Article{" +
                "product=" + product +
                ", quantity=" + quantity +
                '}';
    }
}
