package edu.tucn.isp.lecture4.shoppingcart;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Radu Miron
 * @version 1
 */
public class ShoppingCart {
    private List<Article> articles = new ArrayList<>();

    public void addArticle(Product product, int quantity) {
        Article article = new Article(product,quantity);
        articles.add(article);
    }
}
