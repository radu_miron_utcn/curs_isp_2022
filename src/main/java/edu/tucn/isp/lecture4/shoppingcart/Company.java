package edu.tucn.isp.lecture4.shoppingcart;

/**
 * @author Radu Miron
 * @version 1
 */
public class Company extends Customer {
    private String name;
    private String regNumber;

    public Company(String name, String regNumber, String address, String phone, ShoppingCart shoppingCart) {
        super(address, phone, shoppingCart);
        this.name = name;
        this.regNumber = regNumber;
    }
}
