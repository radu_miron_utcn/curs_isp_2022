package edu.tucn.isp.lecture12.ex1;

import javax.swing.*;

/**
 * @author Radu Miron
 * @version 1
 */
public class GUI extends JFrame {
    private JTextField fileNameInput;
    private JTextArea fileContentContainer;
    private JButton startButton;

    public GUI() {
        setSize(240, 560);
        setLayout(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        fileNameInput = new JTextField("File Name");
        fileNameInput.setBounds(20, 20, 200, 20);

        fileContentContainer = new JTextArea("File Content");
        fileContentContainer.setBounds(20, 60, 200, 400);

        startButton = new JButton("Start");
        startButton.setBounds(20, 480, 200, 20);
        startButton.addActionListener(
                e -> {
                    RefreshThread refreshThread = new RefreshThread();
                    refreshThread.setFileNameInput(fileNameInput);
                    refreshThread.setFileContentContainer(fileContentContainer);
                    refreshThread.start();
                });

        add(fileNameInput);
        add(fileContentContainer);
        add(startButton);

        setVisible(true);
    }
}
