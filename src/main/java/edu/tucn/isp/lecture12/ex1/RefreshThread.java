package edu.tucn.isp.lecture12.ex1;

import org.apache.commons.io.FileUtils;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author Radu Miron
 * @version 1
 */
public class RefreshThread extends Thread {
    private JTextField fileNameInput;
    private JTextArea fileContentContainer;

//    public RefreshThread(JTextField fileNameInput, JTextArea fileContentContainer) {
//        this.fileNameInput = fileNameInput;
//        this.fileContentContainer = fileContentContainer;
//    }

    @Override
    public void run() {
        while (true) {
            String fileName = fileNameInput.getText();
            try {
                String content = FileUtils.readFileToString(new File(fileName), StandardCharsets.UTF_8);
                fileContentContainer.setText(content);
                Thread.sleep(10000);
            } catch (IOException | InterruptedException e) {
                fileContentContainer.setText(e.getMessage());
            }
        }
    }

    public void setFileNameInput(JTextField fileNameInput) {
        this.fileNameInput = fileNameInput;
    }

    public void setFileContentContainer(JTextArea fileContentContainer) {
        this.fileContentContainer = fileContentContainer;
    }
}
