package edu.tucn.isp.lecture12.ex2;

import edu.tucn.isp.lecture9.ex5decoupling.FileUtils;

import javax.swing.*;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * @author Radu Miron
 * @version 1
 */
public class Consumer extends Thread {
    private JTextArea resultsSection;
    private ArrayBlockingQueue<String> taskQueue;

    public Consumer(JTextArea resultsSection, ArrayBlockingQueue<String> taskQueue) {
        this.resultsSection = resultsSection;
        this.taskQueue = taskQueue;
    }

    @Override
    public void run() {
        Map<String, Integer> results = null;
        try {
            String fileName = taskQueue.take();
            results = FileUtils.processFile(fileName);
        } catch (InterruptedException ignored) {
        }
        resultsSection.append("Processed by " + Thread.currentThread().getName() + "\n");
        results.forEach((k, v) ->
                resultsSection.append(k + ": " + v + "\n"));
    }
}
