package edu.tucn.isp.lecture12.ex2;

import edu.tucn.isp.lecture9.ex5decoupling.FileUtils;

import javax.swing.*;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win extends JFrame {
    private ArrayBlockingQueue<String> taskQueue;
    private JTextArea resultsSection;

    Win(ArrayBlockingQueue<String> taskQueue) {
        this.taskQueue = taskQueue;

        setSize(500, 500);
        setLayout(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JLabel fileNameLabel = new JLabel("File name");
        fileNameLabel.setBounds(20, 20, 100, 20);

        JTextField filePathInput = new JTextField();
        filePathInput.setBounds(130, 20, 100, 20);

        JButton processButton = new JButton("Process");
        processButton.setBounds(240, 20, 100, 20);

        resultsSection = new JTextArea();
        resultsSection.setBounds(20, 60, 300, 300);

        processButton.addActionListener(ae -> {
            try {
                taskQueue.put(filePathInput.getText());
            } catch (InterruptedException ignored) {
            }
        });

        add(fileNameLabel);
        add(filePathInput);
        add(processButton);
        add(resultsSection);

        setVisible(true);
    }

    public JTextArea getResultsSection() {
        return resultsSection;
    }
}
