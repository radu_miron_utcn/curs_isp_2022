package edu.tucn.isp.lecture12.ex2;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        ArrayBlockingQueue<String> taskQueue = new ArrayBlockingQueue<>(1000);
        Win win = new Win(taskQueue);
        new Consumer(win.getResultsSection(), taskQueue).start();
        new Consumer(win.getResultsSection(), taskQueue).start();
    }
}
