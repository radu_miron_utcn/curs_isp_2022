package edu.tucn.isp.lecture8.ex2;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static final int NUM_OF_ENEMIES =3;

    public static void main(String[] args) {
        List<Enemy> enemies = new ArrayList<>();

        for (int i=0;i< NUM_OF_ENEMIES;i++){
            enemies.add(new Enemy());
        }

        new Win(enemies);

        // Move the enemies with a different speed; use Thread.sleep() to pause between movements.
        // Use your imagination to create a little game: keep the score; different levels; pause/restart; etc
    }
}
