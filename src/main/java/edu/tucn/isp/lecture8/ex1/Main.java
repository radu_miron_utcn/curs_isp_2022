package edu.tucn.isp.lecture8.ex1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    private static final String CLICK = "Click";
    private static final String CLICK_2 = "Click 2";

    public static void main(String[] args) {
        JFrame frame = new JFrame("My First GUI App");
        frame.setBounds(100, 100, 600, 100);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLayout(new GridLayout(1, 3));

        JButton button1 = new JButton(CLICK);
        JButton button2 = new JButton(CLICK_2);

        JTextField textField = new JTextField();

        button1.addActionListener(new ButtonHandler(null));
        button2.addActionListener(new ButtonHandler(textField));

        frame.add(button1);
        frame.add(button2);
        frame.add(textField);
        frame.setVisible(true);
    }

    static class ButtonHandler implements ActionListener {
        private JTextField textField;

        public ButtonHandler(JTextField textField) {
            this.textField = textField;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            int num = new Random().nextInt(Integer.MAX_VALUE); //DRY

            if(actionEvent.getActionCommand().equals(CLICK)){
                System.out.println(num);
            } else if(actionEvent.getActionCommand().equals(CLICK_2)){
                textField.setText("" + num);
            } else {
                throw new IllegalArgumentException("Unknown click source");
            }
        }
    }

}
