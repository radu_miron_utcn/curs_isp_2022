package edu.tucn.isp.lecture9.ex4lambda;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        new Thread(() -> activity()).start();
        new Thread(() -> activity()).start();
    }

    public static void activity(){
        for (int i = 0; i < 20; i++) {
            System.out.println(Thread.currentThread().getName() +
                    " message " + i);

            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
            }
        }
    }
}
