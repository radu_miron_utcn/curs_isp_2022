package edu.tucn.isp.lecture9.ex1thread;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Thread t1 = new MyThread();
        Thread t2 = new MyThread();

        t1.start();
        t2.start();

        //calls like the next ones will be excuted on the main thread
//        t1.run();
//        t2.run();
    }
}
