package edu.tucn.isp.lecture9.ex6observer;

import javax.swing.*;
import java.awt.*;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win extends JFrame implements Observer {
    private JTextField statusMonitor;

    Win() {
        setSize(200, 50);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        statusMonitor = new JTextField();
        add(statusMonitor);
        setVisible(true);
    }

    @Override
    public void update(Object event) {
        boolean isLocked = (boolean) event;

        if (isLocked) {
            statusMonitor.setBackground(Color.RED);
        } else {
            statusMonitor.setBackground(Color.GREEN);
        }
    }
}
