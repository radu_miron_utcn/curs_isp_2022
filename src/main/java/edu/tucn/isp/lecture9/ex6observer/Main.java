package edu.tucn.isp.lecture9.ex6observer;

import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Door door = new Door();
        Win win = new Win();

        door.register(win);

        for (int i = 0; i < 10; i++) {
            if (i % 2 == 0) {
                door.unlock();
            } else {
                door.lock();
            }

            try {
                Thread.sleep((new Random().nextInt(4) + 1) * 1000);
            } catch (InterruptedException ignored) {
            }
        }
    }
}
