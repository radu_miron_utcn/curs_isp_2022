package edu.tucn.isp.lecture9.ex6observer;

/**
 * @author Radu Miron
 * @version 1
 */
public class Door extends Observable {
    private boolean isLocked;

    public void lock() {
        this.isLocked = true;
        this.changeState(this.isLocked);
    }

    public void unlock() {
        this.isLocked = false;
        this.changeState(this.isLocked);
    }
}
