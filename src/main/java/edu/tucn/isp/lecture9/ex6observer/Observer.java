package edu.tucn.isp.lecture9.ex6observer;

/**
 * @author Radu Miron
 * @version 1
 */
public interface Observer {
    void update(Object event);
}
