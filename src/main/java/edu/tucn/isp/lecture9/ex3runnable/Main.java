package edu.tucn.isp.lecture9.ex3runnable;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        MyRunnableThread2 r1 = new MyRunnableThread2();
        MyRunnableThread2 r2 = new MyRunnableThread2();

        r1.start();
        r2.start();
    }
}
