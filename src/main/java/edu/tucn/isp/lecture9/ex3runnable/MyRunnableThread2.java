package edu.tucn.isp.lecture9.ex3runnable;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyRunnableThread2 implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println(Thread.currentThread().getName() +
                    " message " + i);

            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
            }
        }
    }

    public void start(){
        new Thread(this).start();
    }
}
