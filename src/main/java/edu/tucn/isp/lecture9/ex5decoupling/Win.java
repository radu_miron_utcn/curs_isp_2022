package edu.tucn.isp.lecture9.ex5decoupling;

import javax.swing.*;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win extends JFrame {
    private static final ExecutorService executor = Executors.newFixedThreadPool(3);

    Win() {
        setSize(500, 500);
        setLayout(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JLabel fileNameLabel = new JLabel("File name");
        fileNameLabel.setBounds(20, 20, 100, 20);

        JTextField filePathInput = new JTextField();
        filePathInput.setBounds(130, 20, 100, 20);

        JButton processButton = new JButton("Process");
        processButton.setBounds(240, 20, 100, 20);

        JTextArea resultsSection = new JTextArea();
        resultsSection.setBounds(20, 60, 300, 300);

        processButton.addActionListener(ae -> {
//            new Thread(() -> {
                String filePath = filePathInput.getText();
                Map<String, Integer> results = FileUtils.processFile(filePath);
                results.forEach((k, v) ->
                        resultsSection.append(k + ": " + v + "\n"));
//            }).start();
        });
        add(fileNameLabel);
        add(filePathInput);
        add(processButton);
        add(resultsSection);

        setVisible(true);
    }
}
