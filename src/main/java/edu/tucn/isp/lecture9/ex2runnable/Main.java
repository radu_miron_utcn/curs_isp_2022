package edu.tucn.isp.lecture9.ex2runnable;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Thread t1 = new Thread(new MyRunnableThread());
        Thread t2 = new Thread(new MyRunnableThread());

        t1.start();
        t2.start();
    }
}
