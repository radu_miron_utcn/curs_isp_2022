package edu.tucn.isp.lecture9.ex2runnable;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyRunnableThread implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println(Thread.currentThread().getName() +
                    " message " + i);

            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
            }
        }
    }
}
