package edu.tucn.isp.lecture2.ex1;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Radu Miron
 * @version 1
 */
public class MainTest {

    @Test
    public void testCountA() {
        Assert.assertEquals(3, Main.countA("ana are mere"));
        Assert.assertEquals(0, Main.countA("merele"));
        Assert.assertThrows(NullPointerException.class, () -> Main.countA(null));
    }
}
